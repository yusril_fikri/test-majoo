import 'dart:developer';
import 'dart:io';

import 'package:majootestcase/models/user.dart';
import 'package:path_provider/path_provider.dart';
import 'package:sqflite/sqflite.dart';
import 'package:path/path.dart' as path;

class DBUserHelper {
  static final _dbName = "db.db";
  static final _dbVer = 1;

  static final _table = 'user';

  static final columnId = '_id';
  static final columnUsername = 'username';
  static final columnEmail = 'email';
  static final columnPassword = 'password';

  DBUserHelper._privateConstructor();
  static final DBUserHelper instance = DBUserHelper._privateConstructor();

  static Database? _db;
  Future<Database> get db async {
    if (_db == null) {
      _db = await _initDB();
    }
    return _db!;
  }

  _initDB() async {
    Directory docDir = await getApplicationDocumentsDirectory();
    String docPath = path.join(docDir.path, _dbName);
    return await openDatabase(docPath, version: _dbVer, onCreate: _onCreateDb);
  }

  Future _onCreateDb(Database db, int ver) async {
    await db.execute("""CREATE TABLE user (
        _id INTEGER PRIMARY KEY,
        email TEXT,
        password TEXT,
        username TEXT
      );""");
  }

  Future<int> addUser(User input) async {
    Database db = await instance.db;
    return await db.insert(_table, input.toJson());
  }

  Future<List<User>> getUser(String? email, String? password) async {
    Database db = await instance.db;
    List<Map<String, dynamic>> listBookmark = await db.query(_table,
        columns: [columnId, columnUsername, columnPassword],
        where: '$columnEmail = ? AND $columnPassword = ?',
        whereArgs: [email, password]);

    return listBookmark.map((data) => User.fromJson(data)).toList();
  }
}
