import 'dart:async';
import 'dart:convert';
import 'dart:developer';

import 'package:dio/dio.dart';
import 'package:majootestcase/models/movie.dart';
import 'package:majootestcase/models/movie_response.dart';
import 'package:majootestcase/services/dio_config_service.dart' as dioConfig;

class ApiServices {
  Future<MovieResponse?> getMovieList() async {
    //  try {
    var dio = await dioConfig.dio();
    Response<String> response = await dio!.get(
        "https://api.themoviedb.org/3/trending/all/day?api_key=0bc9e6490f0a9aa230bd01e268411e10");
    log(response.data.toString());
    MovieResponse movieResponse =
        MovieResponse.fromJson(jsonDecode(response.data!));
    return movieResponse;
    // } catch (e) {
    //   log(e.toString());
    //   return null;
    // }
  }
}
