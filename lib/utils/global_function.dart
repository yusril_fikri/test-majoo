import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';

Future nextScreen(BuildContext context, page) {
  return Navigator.push(context, MaterialPageRoute(builder: (_) => page));
}

Future nextReplacementScreen(BuildContext context, page) {
  return Navigator.pushReplacement(
      context, MaterialPageRoute(builder: (_) => page));
}

Future<void> showLoading(BuildContext context) async {
  showDialog<dynamic>(
    context: context,
    barrierDismissible: false,
    builder: (BuildContext dialogContext) {
      dialogContext = context;
      return Material(
        type: MaterialType.transparency,
        child: Center(
          child: Container(
            alignment: Alignment.center,
            child: SpinKitSquareCircle(
              color: Colors.white,
              size: 50.0,
            ),
          ),
        ),
      );
    },
  );
}

Future<void> hideLoading(BuildContext context) async {
  Navigator.pop(context);
}
