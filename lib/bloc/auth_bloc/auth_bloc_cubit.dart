import 'dart:developer';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:majootestcase/models/user.dart';
import 'package:majootestcase/services/db_user.dart';
import 'package:shared_preferences/shared_preferences.dart';

part 'auth_bloc_state.dart';

class AuthBlocCubit extends Cubit<AuthBlocState> {
  AuthBlocCubit() : super(AuthBlocInitialState());

  Future<void> fetch_history_login() async {
    emit(AuthBlocInitialState());
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    bool? isLoggedIn = sharedPreferences.getBool("is_logged_in");
    if (isLoggedIn == null) {
      emit(AuthBlocLoginState());
    } else {
      if (isLoggedIn) {
        emit(AuthBlocLoggedInState());
      } else {
        emit(AuthBlocLoginState());
      }
    }
  }

  void login_user(User user) async {
    try {
      inspect(user);
      emit(AuthBlocLoadingState());
      SharedPreferences sharedPreferences =
          await SharedPreferences.getInstance();
      DBUserHelper db = DBUserHelper.instance;

      List<User> listUser = await db.getUser(user.email!, user.password!);
      log(listUser.toString());
      if (listUser.length > 0) {
        await sharedPreferences.setBool("is_logged_in", true);
        String data = listUser.first.toJson().toString();
        sharedPreferences.setString("user_value", data);
        emit(AuthBlocLoggedInState());
      } else {
        emit(AuthBlocErrorState("User tidak ditemukan"));
      }
    } catch (e) {
      log(e.toString());
      emit(AuthBlocErrorState((e.toString())));
    }
  }

  void register_user(User user) async {
    try {
      log("sampai 1");
      emit(AuthBlocLoadingState());
      SharedPreferences sharedPreferences =
          await SharedPreferences.getInstance();
      DBUserHelper db = DBUserHelper.instance;
      log("sampai 2");
      var respons = await db.addUser(user);
      log("sampai 3 ${respons}");
      if (respons > 0) {
        emit(AuthBlocLoggedInState());
      } else {
        emit(AuthBlocErrorState("Registrasi gagal di inoutkan"));
      }
    } catch (e) {
      log(e.toString());
      emit(AuthBlocErrorState(e.toString()));
    }
  }
}
