import 'dart:developer';

import 'package:another_flushbar/flushbar.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:majootestcase/bloc/auth_bloc/auth_bloc_cubit.dart';
import 'package:majootestcase/bloc/home_bloc/home_bloc_cubit.dart';
import 'package:majootestcase/common/widget/custom_button.dart';
import 'package:majootestcase/common/widget/text_form_field.dart';
import 'package:majootestcase/models/user.dart';
import 'package:majootestcase/ui/home_bloc/home_bloc_screen.dart';
import 'package:majootestcase/ui/login/login_page.dart';
import 'package:majootestcase/utils/global_function.dart';

class RegisterPage extends StatefulWidget {
  @override
  _RegisterState createState() => _RegisterState();
}

class _RegisterState extends State<RegisterPage> {
  final _emailController = TextController();
  final _usernameController = TextController();
  final _passwordController = TextController();
  final _passwordKonfirmasiController = TextController();
  GlobalKey<FormState> formKey = new GlobalKey<FormState>();

  bool _isObscurePassword = true;
  bool _isObscureKonfirmPassword = true;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: BlocListener<AuthBlocCubit, AuthBlocState>(
        listener: (context, state) async {
          if (state is AuthBlocLoggedInState) {
            log("berhasil");
            await hideLoading(context);
            Flushbar(
              title: "Informasi",
              message: "data berhasil disimpan silahkan login ",
              duration: Duration(seconds: 3),
            )..show(context);
            nextScreen(context, LoginPage());
          }
          if (state is AuthBlocLoadingState) {
            showLoading(context);
          }
          if (state is AuthBlocErrorState) {
            hideLoading(context);
            Flushbar(
              title: "Informasi",
              message: state.error,
              duration: Duration(seconds: 3),
            )..show(context);
          }
        },
        child: SingleChildScrollView(
          child: Padding(
            padding: EdgeInsets.only(top: 75, left: 25, bottom: 25, right: 25),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text(
                  'Selamat Datang',
                  style: TextStyle(
                    fontSize: 24,
                    fontWeight: FontWeight.bold,
                    // color: colorBlue,
                  ),
                ),
                Text(
                  'Silahkan registrasi terlebih dahulu',
                  style: TextStyle(
                    fontSize: 15,
                    fontWeight: FontWeight.w400,
                  ),
                ),
                SizedBox(
                  height: 9,
                ),
                _form(),
                SizedBox(
                  height: 50,
                ),
                CustomButton(
                  text: 'Login',
                  onPressed: handleRegister,
                  height: 100,
                ),
                SizedBox(
                  height: 50,
                ),
                _login(),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget _form() {
    return Form(
      key: formKey,
      child: Column(
        children: [
          CustomTextFormField(
            context: context,
            controller: _usernameController,
            isEmail: true,
            hint: 'exp : admin',
            label: 'Username',
          ),
          CustomTextFormField(
            context: context,
            controller: _emailController,
            isEmail: true,
            hint: 'Example@123.com',
            label: 'Email',
            validator: (val) {
              final pattern = new RegExp(r'([\d\w]{1,}@[\w\d]{1,}\.[\w]{1,})');
              if (val != null)
                return pattern.hasMatch(val) ? null : 'email is invalid';
            },
          ),
          CustomTextFormField(
            context: context,
            label: 'Password',
            hint: 'password',
            controller: _passwordController,
            isObscureText: _isObscurePassword,
            suffixIcon: IconButton(
              icon: Icon(
                _isObscurePassword
                    ? Icons.visibility_off_outlined
                    : Icons.visibility_outlined,
              ),
              onPressed: () {
                setState(() {
                  _isObscurePassword = !_isObscurePassword;
                });
              },
            ),
          ),
          CustomTextFormField(
            context: context,
            label: 'Konfirmasi Password',
            hint: 'password',
            controller: _passwordKonfirmasiController,
            isObscureText: _isObscureKonfirmPassword,
            suffixIcon: IconButton(
              icon: Icon(
                _isObscureKonfirmPassword
                    ? Icons.visibility_off_outlined
                    : Icons.visibility_outlined,
              ),
              onPressed: () {
                setState(() {
                  _isObscureKonfirmPassword = !_isObscureKonfirmPassword;
                });
              },
            ),
          ),
        ],
      ),
    );
  }

  Widget _login() {
    return Align(
      alignment: Alignment.center,
      child: TextButton(
        onPressed: () => nextReplacementScreen(context, LoginPage()),
        child: RichText(
          text: TextSpan(
              text: 'sudah punya akun? ',
              style: TextStyle(color: Colors.black45),
              children: [
                TextSpan(
                  text: 'Login',
                ),
              ]),
        ),
      ),
    );
  }

  void handleRegister() async {
    final String? _email = _emailController.value;
    final String? _uname = _usernameController.value;
    final String? _password = _passwordController.value;
    if (formKey.currentState?.validate() == true &&
        _email != null &&
        _password != null) {
      AuthBlocCubit authBlocCubit = AuthBlocCubit();
      User user = User(
        email: _email,
        password: _password,
        userName: _uname,
      );
      authBlocCubit.register_user(user);
    }
  }
}
