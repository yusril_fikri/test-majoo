import 'dart:convert';

class MovieResponse {
  MovieResponse({
    this.data,
    this.page,
    this.totalpages,
    this.totalresults,
  });
  List<Data>? data;
  int? page;
  int? totalpages;
  int? totalresults;
  factory MovieResponse.fromJson(Map<String, dynamic> json) => MovieResponse(
        data: List<Data>.from(json["results"].map((x) => Data.fromJson(x))),
        page: json["page"],
        totalpages: json["total_pages"],
        totalresults: json["total_results"],
      );
  Map<String, dynamic> toJson() => {
        "d": List<dynamic>.from(data!.map((x) => x.toJson())),
        "page": page,
        "total_pages": totalpages,
        "total_results": totalresults,
      };
}

class Data {
  Data({
    this.originallanguage,
    this.originaltitle,
    this.posterpath,
    this.video,
    this.voteaverage,
    this.overview,
    this.releasedate,
    this.votecount,
    this.adult,
    this.backdroppath,
    this.title,
    this.id,
    this.popularity,
    this.genreids,
    this.mediatype,
  });
  String? originallanguage;
  String? originaltitle;
  String? posterpath;
  bool? video;
  double? voteaverage;
  String? overview;
  String? releasedate;
  int? votecount;
  bool? adult;
  String? backdroppath;
  String? title;
  int? id;
  double? popularity;
  List<int?>? genreids;
  String? mediatype;
  factory Data.fromJson(Map<String, dynamic> json) => Data(
        originallanguage: json["original_language"],
        originaltitle: json["original_title"],
        posterpath: json["poster_path"],
        video: json["video"],
        voteaverage: json["vote_average"],
        overview: json["overview"],
        releasedate: json["release_date"],
        votecount: json["vote_count"],
        adult: json["adult"],
        backdroppath: json["backdrop_path"],
        title: json["title"] ?? "",
        id: json["id"],
        popularity: json["popularity"],
        mediatype: json["media_type"],
      );
  Map<String, dynamic> toJson() => {
        "original_language": originallanguage,
        "original_title": originaltitle,
        "poster_path": posterpath,
        "video": video,
        "vote_average": voteaverage,
        "overview": overview,
        "release_date": releasedate,
        "vote_count": votecount,
        "adult": adult,
        "backdrop_path": backdroppath,
        "title": title,
        "id": id,
        "popularity": popularity,
        "media_type": mediatype,
      };
}

// class I {
//   I({
//     this.height,
//     this.imageUrl,
//     this.width,
//   });
//   int? height;
//   String? imageUrl;
//   int? width;
//   factory I.fromJson(Map<String, dynamic> json) => I(
//         height: json["height"],
//         imageUrl: json["imageUrl"],
//         width: json["width"],
//       );
//   Map<String, dynamic> toJson() => {
//         "height": height,
//         "imageUrl": imageUrl,
//         "width": width,
//       };
// }
// // class Series {
//   Series({
//     this.i,
//     this.id,
//     this.l,
//     this.s,
//   });
//   I? i;
//   String? id;
//   String? l;
//   String? s;
//   factory Series.fromJson(Map<String, dynamic> json) => Series(
//     i: I.fromJson(json["i"]),
//     id: json["id"],
//     l: json["l"],
//     s: json["s"],
//   );
//   Map<String, dynamic> toJson() => {
//   "i": i!.toJson(),
//   "id": id,
//   "l": l,
//   "s": s,
// };
// }

















